set linesize 150
set pagesize 15000
set heading off
set termout off
col host_name for a30
select instance_name, host_name, version, startup_time as startup, status from v$instance;
exit;
