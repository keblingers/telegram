#!/bin/bash
#---ORACLE ENVIRONMENT---#
ORACLE_HOME=/apps/oracle/product/11.2.0/dbhome_1;export ORACLE_HOME
ORACLE_SID=vmon; export ORACLE_SID
PATH=$ORACLE_HOME/bin:$PATH
NOW=$(date +"%m-%d-%Y:%T")

#-DIRECTORY LOCATION-#
#LOG=
MONIT=/home/oracle/monitoring/monit2telegram-master; export MONIT
QUERY=/home/oracle/monitoring/query; export QUERY

#Temp File
rm -rf /home/oracle/monitoring/temp/check_db.txt


for x in `cat /home/oracle/monitoring/db_server.lst `
do
x1=`echo ${x} | sed 's/,/ /g' | awk '{print $1}'`
x2=`echo ${x} | sed 's/,/ /g' | awk '{print $2}'`
tnsping_var=`tnsping ${x1} | grep "OK (" | wc -l`
echo tnsping to ${x1} , please wait......
if [ $tnsping_var -eq 1 ]; then
sqlplus -L dba_mon/oracle@${x1} @/home/oracle/monitoring/query/check_db.sql >> /home/oracle/monitoring/temp/check_db.txt #<<EOF
echo ${x1} "databse is up"
#$QUERY/check_up.sh
#sqlplus dba_mon/oracle@${x1} @/home/oracle/monitoring/query/check_db.sql >> /home/oracle/monitoring/temp/check_db.txt #<<EOF
#insert into test_status values(stat_seq.nextval,'${x1}','${x1}',sysdate,'OK');
#commit;
#exit;
#EOF
else
echo ${x1} "database is down in ${x2}"
sqlplus dba_mon/oracle <<EOF
insert into test_status values(stat_seq.nextval,'${x1}','${x1}',sysdate,'DOWN');
commit;
exit;
EOF
/usr/local/bin/sendtelegram.sh -c /etc/telegramrc -m "$NOW ${x1} database is down in ${x2}"
fi
done

echo "INSTANCE_NAME    HOST_NAME                      VERSION           STARTUP   STATUS"
echo "---------------- ------------------------------ ----------------- --------- ------------"
cat /home/oracle/monitoring/temp/check_db.txt
